## Cloning ##

git clone git@bitbucket.org:nikolalozanovski/cubegl.git --recursive

## Compiling ##

Execute file named **cc** as **./cc**

`cd src`

`./cc`

Tested with Ubuntu 20.04, OpenGL 3.3

Compiled with GCC 10.2.0,  C++17 (should work with C++11 or later)

## What is this about? ##

This repo is OpenGL rendering example.

Two cubes will be displayed on screen, one rotating shaded and wireframed, one static

## What is used ##

* Vertex Array objects
* Vertex Buffer objects
* Uniform Buffer objects
* Two shaders, one for rotating cube and one for static cube

## Program instructions ##

Press key **K** to change rotation direction

Press key **F** to enable or disable vertical sync

## Created by ##

Nikola Lozanovski
