#pragma once
#include <depths.hpp>

#include <shader.hpp>
#include <vao.hpp>

enum class RenderingType {
	SHADED,
	WIREFRAMED
};

class Rendering final {
public:
	Rendering() = delete;

	static void setDepth(Depths depth);
	static void setBackgroundColor(float red, float green, float blue, float alpha);
	static void clearScreen();
	static void render(RenderingType renderingType,
		uint64_t elementsCount);
protected:
private:
};