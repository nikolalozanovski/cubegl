#pragma once

#include <vector>
#include <cstdint>

class VertexBufferObject {
public:
	VertexBufferObject();
	~VertexBufferObject();

	void bindVertices(std::vector<float>& verticesContainer);
	void bindColor(std::vector<float>& colorContainer);
	void bindMVP(std::vector<float>& data);

	uint32_t getId();
protected:
private:
	uint32_t vboId;
};