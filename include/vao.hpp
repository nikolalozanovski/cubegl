#pragma once
#include <cstdint>

class VertexArrayObject {
public:
	VertexArrayObject();
	~VertexArrayObject();

	int getVao();
	void bindVao();
protected:
private:
	uint32_t vaoId;
};