#pragma once
#include <string>

#include <vbo.hpp>

class Shader {
public:
	Shader(uint64_t programID);
	~Shader();
	int loadShaders(const std::string& vertex_file_path, const std::string& fragment_file_path);
	void useProgram();

	template <typename T>
	bool setUniformMatrixValue(std::string&& uniformName, T& value) {
		int32_t uniformLocation = getUniformLocation(std::move(uniformName));
		if (uniformLocation != -1) {
			glUniformMatrix4fv(uniformLocation, 1, false, value_ptr(value));
			return true;
		}

		return false;
	}

	bool activateUniformBufferObjectByName(std::string&& uniformName, VertexBufferObject& vbo);
	int32_t getUniformLocation(std::string&& uniformName);
protected:
private:

	std::string vertexShaderCode(const std::string& vertex_file_path);
	std::string fragmentShaderCode(const std::string& fragment_file_path);
	int compileVertexShader(const std::string& vertexShaderSource);
	int compileFragmentShader(const std::string& fragmentShaderSource);
	int checkVertexShader();
	int checkFragmentShader();
	int linkProgram();

	uint64_t programID;
	uint64_t vertexShaderID;
	uint64_t fragmentShaderID;
};
