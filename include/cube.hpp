#pragma once
#include <vector>
#include <GL/glew.h>

class Cube {
public:
	Cube() = delete;

	static std::vector<float> getVertexBufferData();
	static std::vector<float> getColorBufferData();
	static std::vector<float> getBlackBufferData();

	static void changeDirection();
	static float getRotationDirection();
protected:
private:
	static float rotationDirection;
};