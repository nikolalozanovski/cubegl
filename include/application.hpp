#pragma once

#include <window.hpp>

class Application {
public:
	static bool initGLFW();
	static bool initGLEW();
	static void terminateApplication();
	static void setOpenGlContext(Window& window);
	static int createShaderProgram();
	static void checkForEvents();
	static bool getVerticalSyncState();
	static void setVerticalSyncState(bool state);
	static bool shouldApplicationExit();
	static void setApplicationExitFlag(bool exitFlag);
protected:
private:
	static bool vSync;
	static bool shouldExit;
};