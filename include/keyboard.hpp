#pragma once

#include <window.hpp>
#include <key.hpp>

class Keyboard {
public:
	virtual void input(Window& window, Key&& key) = 0;
protected:
private:
};