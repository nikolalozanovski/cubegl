#pragma once

#include <memory>

#include <window.hpp>
#include <keyboard.hpp>

class InputKeyboard {
public:
	InputKeyboard() = delete;

	static void activateKeyboard(Window* window, Keyboard* keyboard);
protected:
private:
	static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
	static Keyboard* keyboard;
	static Window* window;
};