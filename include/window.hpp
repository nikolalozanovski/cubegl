#pragma once

#include <string>
#include <memory>
#include <functional>

#include <GLFW/glfw3.h>

class Window {
public:
	Window();
	Window(uint64_t height, uint64_t width, std::string&& title);
	Window(const Window& window) = delete;
	Window(Window&& window);
	~Window();
	bool shouldWindowClose();
	GLFWwindow* getWindowHandle();
	void open(uint64_t height, uint64_t width, std::string&& title);
	void swapBuffers();
protected:
private:
	void initGlfwWindowHints();
	static void windowSizeCallback(GLFWwindow* currentWindow, int width, int height);
	uint64_t height;
	uint64_t width;
	std::string title;
	std::unique_ptr<GLFWwindow, std::function<void(GLFWwindow*)>> window;
};