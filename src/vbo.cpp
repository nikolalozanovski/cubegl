#include <vbo.hpp>

#include <GL/glew.h>

VertexBufferObject::VertexBufferObject() {
	glGenBuffers(1, &vboId);
}

VertexBufferObject::~VertexBufferObject() {
	glDeleteBuffers(1, &vboId);
	glDisableVertexAttribArray(vboId - 1);

}

uint32_t VertexBufferObject::getId() {
	return vboId;
}

void VertexBufferObject::bindVertices(std::vector<float>& verticesContainer) {
	glBindBuffer(GL_ARRAY_BUFFER, vboId);
	glBufferData(GL_ARRAY_BUFFER, 
		sizeof(float) * verticesContainer.size(), 
		verticesContainer.data(), 
		GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
		glEnableVertexAttribArray(0); // location 0 in the shader are the vertices
}

void VertexBufferObject::bindColor(std::vector<float>& colorContainer) {
	glBindBuffer(GL_ARRAY_BUFFER, vboId);
	glBufferData(GL_ARRAY_BUFFER, 
		sizeof(float) * colorContainer.size(), 
		colorContainer.data(), 
		GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glEnableVertexAttribArray(1); // location 1 in the shader is the color
}

void VertexBufferObject::bindMVP(std::vector<float>& data) {
	glBindBuffer(GL_UNIFORM_BUFFER, vboId);
	glBufferData(GL_UNIFORM_BUFFER, 
		sizeof(float) * data.size(), 
		data.data(),
		GL_STATIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}