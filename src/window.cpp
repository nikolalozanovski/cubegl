#include <window.hpp>

#include <utility>

void Window::windowSizeCallback(GLFWwindow* currentWindow, int width, int height) {
	// this->width = width;
	// this->height = height;
	glViewport(0, 0, width, height);
}

Window::Window() {
	initGlfwWindowHints();
}

Window::Window(uint64_t height, uint64_t width, std::string&& title) : height(height),
width(width), title(title) {
	initGlfwWindowHints();
	open(height, width, std::move(title));
}
Window::~Window() {
	if (window) {
		window.release();
	}
}

bool Window::shouldWindowClose() {
	return glfwWindowShouldClose(getWindowHandle()) == 0;
}

GLFWwindow* Window::getWindowHandle() {
	return window.get();
}

Window::Window(Window&& window) : height(window.height),
width(window.width), title(window.title) {
	this->window.swap(window.window);
}

void Window::open(uint64_t height, uint64_t width, std::string&& title) {
	window = std::unique_ptr<GLFWwindow, std::function<void(GLFWwindow*)>>(glfwCreateWindow(height,
		width,
		title.c_str(),
		NULL,
		NULL),
	[](GLFWwindow* currentWindow) {
		glfwDestroyWindow(currentWindow);
	});
	glfwSetInputMode(getWindowHandle(), GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetWindowSizeCallback(getWindowHandle(), windowSizeCallback);
}

void Window::initGlfwWindowHints() {
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

}

void Window::swapBuffers() {
	glfwSwapBuffers(getWindowHandle());
}
