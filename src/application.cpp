#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <application.hpp>

bool Application::vSync;
bool Application::shouldExit;

bool Application::initGLFW() {
	return glfwInit();
}

bool Application::initGLEW() {
	return glewInit() == GLEW_OK;
}

void Application::terminateApplication() {
	glfwTerminate();
}
void Application::setOpenGlContext(Window& window) {
	glfwMakeContextCurrent(window.getWindowHandle());	
}

int Application::createShaderProgram() {
	return glCreateProgram();
}

void Application::checkForEvents() {
	glfwPollEvents();
}

bool Application::getVerticalSyncState() {
	return Application::vSync;
}

void Application::setVerticalSyncState(bool state) {
	Application::vSync = state;
}

bool Application::shouldApplicationExit() {
	return Application::shouldExit;
}

void Application::setApplicationExitFlag(bool exitFlag) {
	Application::shouldExit = exitFlag;
}
