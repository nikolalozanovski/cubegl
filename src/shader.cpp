#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sstream>
using namespace std;

#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>

#include <shader.hpp>

Shader::Shader(uint64_t programID) : programID(programID),
vertexShaderID(glCreateShader(GL_VERTEX_SHADER)),
fragmentShaderID(glCreateShader(GL_FRAGMENT_SHADER)) {}

Shader::~Shader() {
	glDetachShader(programID, vertexShaderID);
	glDetachShader(programID, fragmentShaderID);
	
	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);
}

bool Shader::activateUniformBufferObjectByName(std::string&& uniformName, VertexBufferObject& vbo) {
	uint32_t uniformLocation = glGetUniformBlockIndex(programID, uniformName.c_str());
	glUniformBlockBinding(programID, uniformLocation, 0);
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, vbo.getId()); 
	return uniformLocation != -1;
}

int32_t Shader::getUniformLocation(std::string&& uniformName) {
	return glGetUniformLocation(programID, uniformName.c_str());
}

int Shader::loadShaders(const std::string& vertex_file_path, const std::string& fragment_file_path){

	std::string vertexShaderSource = vertexShaderCode(vertex_file_path);
	std::string fragmentShaderSource = fragmentShaderCode(fragment_file_path);

	if (compileVertexShader(vertexShaderSource) != 1) {
		return 10;
	}
	if (compileFragmentShader(fragmentShaderSource) != 1) {
		return 11;
	}
	if (linkProgram() != 1) {
		return 12;
	}

	return 0;
}


std::string Shader::vertexShaderCode(const std::string& vertex_file_path) {
	std::string vertexShaderCode;
	std::ifstream vertexShaderStream(vertex_file_path, std::ios::in);
	if(vertexShaderStream.is_open()){
		std::stringstream sstr;
		sstr << vertexShaderStream.rdbuf();
		vertexShaderCode = sstr.str();
		vertexShaderStream.close();
	}

	return vertexShaderCode;
}

std::string Shader::fragmentShaderCode(const std::string& fragment_file_path) {
	std::string fragmentShaderCode;
	std::ifstream fragmentShaderStream(fragment_file_path, std::ios::in);
	if(fragmentShaderStream.is_open()){
		std::stringstream sstr;
		sstr << fragmentShaderStream.rdbuf();
		fragmentShaderCode = sstr.str();
		fragmentShaderStream.close();
	}

	return fragmentShaderCode;
}

int Shader::compileVertexShader(const std::string& vertexShaderSource) {
	std::cout<<"Compiling vertex shader\n";
	auto vert = vertexShaderSource.c_str();
	glShaderSource(vertexShaderID, 1, &vert , NULL);
	glCompileShader(vertexShaderID);
	return checkVertexShader();
}

int Shader::checkVertexShader() {
	GLint result = GL_FALSE;
	int InfoLogLength;
	glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(vertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(vertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		std::cout<<&VertexShaderErrorMessage[0]<<"\n";
	}
	return result;
}

int Shader::compileFragmentShader(const std::string& fragmentShaderSource) {
	std::cout<<"Compiling fragment shader\n";
	auto frag = fragmentShaderSource.c_str();
	glShaderSource(fragmentShaderID, 1, &frag , NULL);
	glCompileShader(fragmentShaderID);
	return checkFragmentShader();
}

int Shader::checkFragmentShader() {
	GLint result = GL_FALSE;
	int InfoLogLength;
	glGetShaderiv(fragmentShaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(fragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(fragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		std::cout<<&FragmentShaderErrorMessage[0]<<"\n";
	}
	return result;
}

int Shader::linkProgram() {
	GLint result = GL_FALSE;
	int InfoLogLength;
	std::cout<<"Linking program\n";
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);
	glLinkProgram(programID);

	// Check the program
	glGetProgramiv(programID, GL_LINK_STATUS, &result);
	glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> ProgramErrorMessage(InfoLogLength+1);
		glGetProgramInfoLog(programID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}

	return result;
}

void Shader::useProgram() {
	glUseProgram(programID);
}