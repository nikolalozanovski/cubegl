#include <inputKeyboard.hpp>
#include <GLFW/glfw3.h>

Keyboard* InputKeyboard::keyboard;
Window* InputKeyboard::window;

void InputKeyboard::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	keyboard->input(*InputKeyboard::window, {key, action, mods});
}

void InputKeyboard::activateKeyboard(Window* window, Keyboard* keyboard) {
	InputKeyboard::keyboard = keyboard;
	InputKeyboard::window = window;
	glfwSetKeyCallback(window->getWindowHandle(), keyCallback);
}