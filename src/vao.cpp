#include <vao.hpp>

#include <GL/glew.h>

VertexArrayObject::VertexArrayObject() {
	glGenVertexArrays(1, &vaoId);
}

VertexArrayObject::~VertexArrayObject() {
	glDeleteVertexArrays(1, &vaoId);
}

int VertexArrayObject::getVao() {
	return vaoId;
}

void VertexArrayObject::bindVao() {
	glBindVertexArray(vaoId);
}
