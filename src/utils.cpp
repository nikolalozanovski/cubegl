#include <iostream>

#include <utils.hpp>

#include <GLFW/glfw3.h>

void Utils::vSyncState(bool state){
	glfwSwapInterval(state);
}