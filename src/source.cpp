#include <chrono>
#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/matrix_transform.hpp>
#include <glm/glm/gtc/type_ptr.hpp>

#include <shader.hpp>
#include <cube.hpp>
#include <vao.hpp>
#include <vbo.hpp>
#include <window.hpp>
#include <application.hpp>
#include <keyboard.hpp>
#include <inputKeyboard.hpp>
#include <rendering.hpp>
#include <utils.hpp>

class BasicKeyboard : public Keyboard {
public:
	void input(Window& window, Key&& key) {
		if (key.id == Key::ids::KEY_K && key.action == Key::actions::PRESS) {
			Cube::changeDirection();
		} else if ((key.id == Key::ids::KEY_ESCAPE && key.action == Key::actions::PRESS)) {
			Application::setApplicationExitFlag(true);
		} else if ((key.id == Key::ids::KEY_F&& key.action == Key::actions::PRESS)) {
			bool vSyncState = !Application::getVerticalSyncState();
			Application::setVerticalSyncState(vSyncState);
			Utils::vSyncState(vSyncState);
		}
	}
protected:
private:
};

std::vector<float> createVectorFromMatrix(glm::mat4 matrix) {
	return std::vector<float>{
		matrix[0][0], matrix[0][1], matrix[0][2], matrix[0][3],
		matrix[1][0], matrix[1][1], matrix[1][2], matrix[1][3],
		matrix[2][0], matrix[2][1], matrix[2][2], matrix[2][3],
		matrix[3][0], matrix[3][1], matrix[3][2], matrix[3][3]
	};
}

int main( void ) {
	Application app;
	app.initGLFW();

	Window window(800, 600, "Cube OpenGL example");
	app.setOpenGlContext(window);
	app.initGLEW();
	
	int dynamicShaderprogram = app.createShaderProgram();
	int staticShaderProgram = app.createShaderProgram();

	Shader cubeShader(dynamicShaderprogram);
	Shader staticCubeShader(staticShaderProgram);
	
	cubeShader.loadShaders("cube.verts", "cube.frags");
	staticCubeShader.loadShaders("static_cube.verts", "cube.frags");
	glm::mat4 projection = glm::perspective(glm::radians(90.0f), 4.0f / 3.0f, 0.1f, 100.0f);
	glm::mat4 view = glm::lookAt(
		glm::vec3(0, 0, 6), // Camera in World Space
		glm::vec3(0, 0, 0), // Where to look
		glm::vec3(0, 1, 0) // Up camera direction
		);
	glm::mat4 model = glm::mat4(1.0f);
	glm::mat4 MVP = projection * view * model; 

	std::vector<float> vertexBufferData = Cube::getVertexBufferData();
	std::vector<float> colorBufferData = Cube::getColorBufferData();
	//black color is by default
	std::vector<float> wireframeBlackColorData = Cube::getBlackBufferData();

	VertexArrayObject cubeVao;
	cubeVao.bindVao();
	
	VertexBufferObject cubeVerticesData;
	VertexBufferObject cubeColorData;
	VertexBufferObject staticCubeMVP;

	cubeVerticesData.bindVertices(vertexBufferData);
	cubeColorData.bindColor(colorBufferData);

	glm::mat4 staticModel = glm::translate(glm::mat4(1.0f), glm::vec3(2.5f, 2.5f, 0.0f));
	glm::mat4 MVPstatic =  projection * view * staticModel;
	std::vector<float> vecf = createVectorFromMatrix(MVPstatic);
	staticCubeMVP.bindMVP(vecf);
	staticCubeShader.activateUniformBufferObjectByName("MVP", staticCubeMVP);
	
	VertexArrayObject wireframeCubeVao;
	wireframeCubeVao.bindVao();

	VertexBufferObject wireframeCubeData;
	VertexBufferObject wireframeColorData;

	wireframeCubeData.bindVertices(vertexBufferData);
	wireframeColorData.bindColor(wireframeBlackColorData);

	const float rotate = 2;
	float deltaTime {1};
	uint64_t itemsToRender {vertexBufferData.size()};
	
	BasicKeyboard k;
	InputKeyboard::activateKeyboard(&window, &k);

	Rendering::setBackgroundColor(0.5, 0.5, 0.5, 0.0);
	Rendering::setDepth(Depths::LESS);
	while (!Application::shouldApplicationExit() && window.shouldWindowClose()) {
		auto start = std::chrono::high_resolution_clock::now();
		Rendering::clearScreen();
		app.checkForEvents();
		MVP = glm::rotate(MVP, rotate * deltaTime, glm::vec3(0.0f, Cube::getRotationDirection(), 0.0f));
		
		cubeShader.useProgram();
		cubeVao.bindVao();
		cubeShader.setUniformMatrixValue("MVP", MVP);
		Rendering::render(RenderingType::SHADED, itemsToRender);

		wireframeCubeVao.bindVao();
		Rendering::render(RenderingType::WIREFRAMED, itemsToRender);

		staticCubeShader.useProgram();
		cubeVao.bindVao();
		Rendering::render(RenderingType::SHADED, itemsToRender);

		window.swapBuffers();

		auto end = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> diff = end-start;
		deltaTime = diff.count();
	}
	app.terminateApplication();
}
