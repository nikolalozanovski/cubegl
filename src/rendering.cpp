#include <rendering.hpp>

#include <GL/glew.h>

void Rendering::setDepth(Depths depth) {
	glEnable(GL_DEPTH_TEST);
	switch (depth) {
		case Depths::NEVER:
		glDepthFunc(GL_NEVER); 
		break;
		case Depths::LESS:
		glDepthFunc(GL_LESS); 
		break;
		case Depths::EQUAL:
		glDepthFunc(GL_EQUAL); 
		break;
		case Depths::LEQUAL:
		glDepthFunc(GL_LEQUAL); 
		break;
		case Depths::GREATER:
		glDepthFunc(GL_GREATER); 
		break;
		case Depths::NOTEQUAL:
		glDepthFunc(GL_NOTEQUAL); 
		break;
		case Depths::GEQUAL:
		glDepthFunc(GL_GEQUAL); 
		break;
		case Depths::ALWAYS:
		glDepthFunc(GL_ALWAYS); 
		break;
		default:
		glDepthFunc(GL_ALWAYS); 
	}
}

void Rendering::setBackgroundColor(float red, float green, float blue, float alpha) {
	glClearColor(red, green, blue, alpha);
}

void Rendering::clearScreen() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Rendering::render(RenderingType renderingType,
	uint64_t elementsCount) {
	switch(renderingType) {
		case RenderingType::WIREFRAMED:
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		break;
		case RenderingType::SHADED:
		default:
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		break;
	}
	glDrawArrays(GL_TRIANGLES, 0, elementsCount);
}